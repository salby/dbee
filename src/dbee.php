<?php
namespace salby\dbee;
use \PDO;

class dbee {

    public $db;

    /**
     * # DBee constructor
     *
     * ## Config
     * ·Required
     *
     * __String__ ·*host* -- MySQL database host.
     *
     * __String__ ·*database* -- Database/schema name.
     *
     * __String__ ·*user*
     *
     * __String__ ·*password*
     *
     * __String__ *charset*
     *
     * ## Examples
     *
     * ```
     * $dbee = new dbee([
     *     'host' => 'localhost',
     *     'database' => 'test',
     *     'user' => 'root',
     *     'password' => 'abc123'
     * ]);
     * ```
     *
     * @param array $config
     */
    public function __construct($config = []) {
        $defaults = [
            'charset' => 'utf8mb4'
        ];
        $config = array_merge($defaults, $config);

        // Set DSN config.
        $dsn = "mysql:host=$config[host];";

        $host_contains_port = substr_count(':', $config['host']);
        if ($host_contains_port) {
          $dsn = "port=".explode(':', $config['host'])[1].";";
        } else if (isset($config['port'])) {
          $dsn .= "port=".$config['port'].";";
        }

        $dsn .= "dbname=$config[database];charset=$config[charset]";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false
        ];

        // Initialize PDO and connect to database.
        try {
            $pdo = new PDO($dsn, $config['user'], $config['password'], $options);
            $this -> db = $pdo;
        } catch (\PDOException $e) {
            throw new \PDOException($e -> getMessage(), (int)$e -> getCode());
        }

    }

    /**
     * # DBee -- fetch
     * Fetches rows from database.
     *
     * ## Examples
     *
     * ### Fetch without parameters
     * ```php
     * $dbee = new dbee([...]);
     * $users = $dbee -> fetch("SELECT * FROM user");
     * ```
     *
     * ### Fetch with parameters
     * ```php
     * $dbee = new dbee([...]);
     * $params = ['id' => 2];
     * $sql = "SELECT * FROM user WHERE id = :id";
     * $user = $dbee -> fetch($sql, $params);
     * ```
     *
     * @param string $sql
     * @param array $params
     *
     * @return array
     */
    public function fetch($sql, $params = []) {

        // Prepare query if parameters are set.
        if (count($params)) {

            // Prepare, execute and return result from database.
            $query = $this -> db -> prepare($sql);
            $query -> execute($params);
            return $query -> fetchAll();

        } else {

            // Return result from database.
            return $this -> db -> query($sql) -> fetchAll();

        }

    }

    public function query($sql, $params = []) {

        $stmt = $this -> db -> prepare($sql);
        $stmt -> execute($params);
        return $this -> db -> lastInsertId();

    }

}
